using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Ability
{
    [SerializeField] float activationTime;
    [SerializeField] string bulletType;

    protected override void Activate()
    {
        StartCoroutine(ActivateMine());
    }

    IEnumerator ActivateMine()
    {
        GameObject mine = PoolManager.Instance.Get(bulletType);

        mine.SetActive(true);
        mine.transform.position = player.transform.position;

        yield return new WaitForSeconds(activationTime);

        mine.GetComponent<MineController>().SetTeam(player.GetTeam());
        mine.GetComponent<MineController>().SetActive(true);
    }
}
