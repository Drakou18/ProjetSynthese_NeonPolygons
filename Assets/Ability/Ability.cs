using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class Ability : MonoBehaviour
{
    [SerializeField] protected float maxCooldown;
    protected float currentCooldown;
    protected PlayerInput controls;
    protected Player player;
    protected GameObject platform;
    protected Rigidbody2D rb;

    void Awake()
    {
        currentCooldown = 0f;
        player = GetComponent<Player>();
        controls = GetComponentInParent<PlayerInput>();
        rb = GetComponent<Rigidbody2D>();   
    }

    protected virtual void Update()
    {
        currentCooldown = Mathf.MoveTowards(currentCooldown, 0f, Time.deltaTime);

        if(CanActivate())
        {
            currentCooldown = maxCooldown;
            Activate();
        }
    }

    public float GetCooldownRatio() {
        return currentCooldown / maxCooldown;
    }

    protected virtual void Activate()
    {
    }

    protected virtual bool CanActivate()
    {
        
        return currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered;
    }
}