using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : Ability
{
    [SerializeField] float normalLockDuration;
    [SerializeField] float maxLockDuration;
    [SerializeField] int averageNbPixel;
    [SerializeField] Sprite lockSprite;
    [SerializeField] float normalLockSpriteScale;
    private GameObject selectedPlatform;
    [SerializeField] private float lockedOpacity = 0.25f;
	[SerializeField] private string lockSpriteGameObjectName = "LockSprite";
    private float normalOpacity = 1.0f;

    protected override void Activate()
    {
        StartCoroutine(LockPlaformCoroutine(selectedPlatform));
    }

    private IEnumerator LockPlaformCoroutine(GameObject platform)
    {
        Collider2D collider = platform.GetComponent<Collider2D>();
        Rigidbody2D rb = platform.GetComponent<Rigidbody2D>();
        SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();
		Color currentColor = spriteRenderer.color;

		if (!selectedPlatform.GetComponent<DestroyablePlatform>().GetIsLocked())
        {
			rb.constraints = RigidbodyConstraints2D.FreezeAll;
			destroyablePlatform.SetLocked(true);
			
			currentColor.a = lockedOpacity;
			spriteRenderer.color = currentColor;


			GameObject lockIcon = new GameObject(lockSpriteGameObjectName);
			float lockSpriteScale = normalLockSpriteScale / ((float)Mathf.Sqrt(averageNbPixel) / Mathf.Sqrt(destroyablePlatform.GetPixelCount()));
			lockIcon.transform.localScale = new Vector2(lockSpriteScale, lockSpriteScale);
			lockIcon.transform.SetParent(platform.transform);
			SpriteRenderer additionalSpriteRenderer = lockIcon.AddComponent<SpriteRenderer>();
			additionalSpriteRenderer.sprite = lockSprite;
			additionalSpriteRenderer.sortingOrder = 1;
			lockIcon.transform.position = collider.bounds.center;

			float lockDuration = normalLockDuration * (float)Mathf.Sqrt(averageNbPixel) / Mathf.Sqrt(destroyablePlatform.GetPixelCount());
			if (lockDuration > maxLockDuration) lockDuration = maxLockDuration;
			float elapsedTime = 0f;
			while (elapsedTime < lockDuration)
			{
				elapsedTime += Time.deltaTime;
				yield return null;
			}

			Destroy(lockIcon);

			currentColor.a = normalOpacity;
			spriteRenderer.color = currentColor;
			destroyablePlatform.SetLocked(false);
			rb.constraints = RigidbodyConstraints2D.None;
			rb.constraints = RigidbodyConstraints2D.FreezeRotation;
		}
        else
        {
			Destroy(platform.transform.Find(lockSpriteGameObjectName).gameObject);
			spriteRenderer.color = Color.white;
			destroyablePlatform.SetLocked(false);
			rb.constraints = RigidbodyConstraints2D.None;
			rb.constraints = RigidbodyConstraints2D.FreezeRotation;
		}

    }

    protected override bool CanActivate()
    {
        if (currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered)
        {
            selectedPlatform = player.getSelectedPlatform();
            return selectedPlatform != null;
        }
        return false;
    }
}
