using UnityEngine;

public class Munch : Ability
{
    [SerializeField] float maxNbPixel;
    private GameObject selectedPlatform;

    protected override void Activate()
    {
        float healAmount = Mathf.Sqrt(selectedPlatform.GetComponent<DestroyablePlatform>().GetPixelCount());
        player.Heal(healAmount);
        Destroy(selectedPlatform);
    }

    protected override bool CanActivate()
    {
        if (currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered)
        {
            selectedPlatform = player.getSelectedPlatform();
            if (selectedPlatform != null && selectedPlatform.GetComponent<DestroyablePlatform>().GetPixelCount() <= maxNbPixel)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}