using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Taser : Ability
{
    [SerializeField] string bulletType;
    [SerializeField] float activationTime;

    protected override void Activate()
    {
        StartCoroutine(ActivateTaser());
    }

    IEnumerator ActivateTaser()
    {
        GameObject electricity = PoolManager.Instance.Get(bulletType);

        electricity.SetActive(true);
        electricity.transform.position = player.transform.position;
        electricity.GetComponent<ElectricityController>().SetTeam(player.GetTeam());

        yield return new WaitForSeconds(activationTime);

        PoolManager.Instance.ReturnToPool(bulletType, electricity);
    }
}