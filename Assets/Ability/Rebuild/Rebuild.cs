using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine;

public class Rebuild : Ability
{
    [SerializeField] private float movingOpacity = 0.5f;

    private DestroyablePlatform selectedPlatform;
    private float staticOpacity = 1.0f;

    protected override void Activate()
    {
        List<GameObject> allPlatforms = GameObject.FindGameObjectsWithTag(Harmony.Tags.Ground).ToList();

        foreach (GameObject platform in allPlatforms)
        {
            DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();

			if (destroyablePlatform.GetOriginalPosition().Equals(selectedPlatform.GetOriginalPosition()) && !destroyablePlatform.GetIsLocked())
            {
                StartCoroutine(MovePlatformsCoroutine(platform));
            }
        }
    }

    private IEnumerator MovePlatformsCoroutine(GameObject platform)
    {
        Rigidbody2D rb = platform.GetComponent<Rigidbody2D>();
        SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();
        Vector2 originalPosition = destroyablePlatform.GetOriginalPosition();

        Color currentColor = spriteRenderer.color;
        rb.gravityScale = 0;
        currentColor.a = movingOpacity;
        spriteRenderer.color = currentColor;
        float elapsedTime = 0f;
        Vector2 startingPosition = platform.transform.position;
        float startingAngle = platform.transform.eulerAngles.z;

        while (elapsedTime < Mathf.Min(3, maxCooldown / 2f))
        {
            elapsedTime += Time.deltaTime;
            platform.transform.position = Vector2.Lerp(startingPosition, originalPosition, elapsedTime / (Mathf.Min(3, maxCooldown / 2f) / 2f));
            platform.transform.eulerAngles = new(0, 0, Mathf.LerpAngle(startingAngle, 0, elapsedTime / (Mathf.Min(3, maxCooldown / 2f) / 2f)));
            yield return null;
        }

        rb.gravityScale = 5;
        currentColor.a = staticOpacity;
        spriteRenderer.color = currentColor;

        if(!platform.GetInstanceID().Equals(selectedPlatform.gameObject.GetInstanceID()))
        {
            Destroy(platform);
        }
        else
        {
            selectedPlatform.SetOriginalTexture();
        }
    }

    protected override bool CanActivate()
    {
        if (currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered)
        {
            GameObject platform = player.getSelectedPlatform();
            if (platform == null || platform.GetComponent<DestroyablePlatform>().GetIsLocked()) return false;
            else selectedPlatform = platform.GetComponent<DestroyablePlatform>();
            return true;
        }
        return false;
    }
}