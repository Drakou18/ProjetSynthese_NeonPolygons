using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : Ability
{
    [SerializeField] private GameObject buildPlatform;

    protected override void Activate()
    {
 
        if (buildPlatform != null)
        {
            Vector2 bottomPosition = new Vector2(transform.position.x, transform.position.y - 5.0f);

            Instantiate(buildPlatform, bottomPosition, Quaternion.identity);
        }
    }

    protected override bool CanActivate()
    {
        if (currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered)
        {
            return  !player.IsGrounded();
        }
        return false;
    }
}
