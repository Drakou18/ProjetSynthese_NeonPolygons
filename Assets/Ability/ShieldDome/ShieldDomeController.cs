using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class ShieldDomeController : MonoBehaviour
{
    [SerializeField] float shieldHealth;
    private Color team;

    private void Start()
    {
    }

    public void SetTeam(Color team)
    {
        this.team = team;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet && bullet.GetTeam() != team)
            {
                PoolManager.Instance.ReturnToPool(bullet.GetPoolTag(), bullet.gameObject);
            }
            
            GrenadeProjectile grenade = collision.gameObject.GetComponent<GrenadeProjectile>();
            C4Projectile c4 = collision.gameObject.GetComponent<C4Projectile>();

            if ((grenade && grenade.GetTeam() != team) || (c4 && c4.GetTeam() != team))
            {
                Vector2 reflectionAxis = (collision.gameObject.transform.position - transform.position).normalized;
                Vector2 result = Vector2.Reflect(collision.GetComponent<Rigidbody2D>().velocity, reflectionAxis);
                collision.GetComponent<Rigidbody2D>().velocity = result * .98f;
            }
        }
    }
}
