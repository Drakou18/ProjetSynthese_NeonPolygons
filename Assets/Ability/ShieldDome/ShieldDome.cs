using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldDome : Ability
{
    [SerializeField] float activationTime;
    [SerializeField] string prefabType;

    protected override void Activate()
    {
        StartCoroutine(ActivateShield());
    }

    IEnumerator ActivateShield()
    {
        GameObject shield = PoolManager.Instance.Get(prefabType);

        shield.SetActive(true);
        shield.transform.position = player.transform.position;
        shield.GetComponentInChildren<ShieldDomeController>().SetTeam(player.GetTeam());

        yield return new WaitForSeconds(activationTime);

        PoolManager.Instance.ReturnToPool(prefabType, shield);
    }
}
