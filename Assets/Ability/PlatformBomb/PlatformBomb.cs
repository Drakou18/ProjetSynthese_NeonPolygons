using System.Collections;
using UnityEngine;

public class PlatformBomb : Ability
{
    [SerializeField] private float normalCountdownDuration;
    [SerializeField] private float maxCountdownDuration;
    [SerializeField] private int averageNbPixel;
    [SerializeField] private int maxNbPixel;
    [SerializeField] private float explosionPower;
    [SerializeField] private float explosionDamage;
    [SerializeField] private float flashAcceleration = 0.5f;
    [SerializeField] private Color flashColor = new Color(1f, 0f, 0f);
    private GameObject selectedPlatform;
    private float flashBuffer = 0.005f;

    protected override void Activate()
    {
        StartCoroutine(PlatformBombCoroutine(selectedPlatform));
    }

    private IEnumerator PlatformBombCoroutine(GameObject platform)
    {
        Collider2D collider = platform.GetComponent<Collider2D>();
        SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();


        float flashDuration = normalCountdownDuration * (float)Mathf.Sqrt(averageNbPixel) / Mathf.Sqrt(destroyablePlatform.GetPixelCount());
        if (flashDuration > maxCountdownDuration) flashDuration = maxCountdownDuration;
        float elapsedTime = flashDuration;
        bool isFlash = true;
        while (elapsedTime > flashBuffer)
        {
            elapsedTime -= flashAcceleration * (elapsedTime / flashDuration);

            if(isFlash)
            {
                spriteRenderer.color = flashColor;
            }
            else
            {
                spriteRenderer.color = Color.white;
            }

            isFlash = !isFlash;

            yield return new WaitForSeconds(flashAcceleration * (elapsedTime / flashDuration));
        }

        spriteRenderer.color = Color.white;

        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
        explosion.gameObject.SetActive(true);
        explosion.transform.position = collider.bounds.center;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(player.GetTeam());
        float explosionRadius = Mathf.Min(collider.bounds.size.x, collider.bounds.size.y);
        explosion.Explode(explosionRadius, new Vector2(0, 0));
    }

    protected override bool CanActivate()
    {
        if (currentCooldown <= 0f && controls.actions[Harmony.InputActions.Ability].triggered)
        {
            selectedPlatform = player.getSelectedPlatform();
            if(selectedPlatform != null && selectedPlatform.GetComponent<DestroyablePlatform>().GetPixelCount() <= maxNbPixel)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}
