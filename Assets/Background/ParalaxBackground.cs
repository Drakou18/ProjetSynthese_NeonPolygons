using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxBackground : MonoBehaviour
{
    [SerializeField] private float paralaxSpeed = 1f;
    [SerializeField] private Material paralaxMaterial;

    void Update()
    {
        paralaxMaterial.mainTextureOffset = new Vector2(Time.time * paralaxSpeed, 0);
    }
}
