using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BackHolding : MonoBehaviour
{

    [SerializeField] private GameObject holdImage;
    [SerializeField] private float timeToHold = 2;
    private float currentTimeHolding = 0;
    private float initialPosition;
    private float finalPosition;

    private void Start()
    {
        initialPosition =  holdImage.transform.position.x;
        finalPosition = initialPosition - 16;
    }


    // Update is called once per frame
    void Update()
    {
        if ((Gamepad.current != null && Gamepad.current.bButton.isPressed) || Keyboard.current.escapeKey.isPressed)
        {
            currentTimeHolding += Time.deltaTime;
            MoveIcon();
            if (currentTimeHolding >= timeToHold)
            {
                AsyncOperation sceneLoad = SceneManager.LoadSceneAsync("Scenes/MainMenuScene");
                sceneLoad.completed += OnSceneLoaded;

            }
        }
        else if (currentTimeHolding != 0)
        {
            currentTimeHolding = 0;
            ResetIcon();
        }
    }

    void OnSceneLoaded(AsyncOperation asyncOperation)
    {
        if (PoolManager.Instance != null)
        {
            Destroy(PoolManager.Instance.gameObject);
        }
    }
    private void MoveIcon()
    {
        float t = currentTimeHolding / timeToHold;
        holdImage.transform.position = new Vector3(Mathf.Lerp(initialPosition, finalPosition, t), holdImage.transform.position.y, holdImage.transform.position.z);
    }

    private void ResetIcon()
    {
        holdImage.transform.position = new Vector3(initialPosition, holdImage.transform.position.y,holdImage.transform.position.z);
    }
}
