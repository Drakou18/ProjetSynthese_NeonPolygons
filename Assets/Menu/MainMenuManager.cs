using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject achievementsPanel;

    private List<GameObject> panels;
    private EventSystem eventSystem;
    private GameObject currentPanel;
    private GameObject mainPanelButtonClicked;
    private bool isUsingMouse = true;

    private void Start()
    {
        currentPanel = mainPanel;
        panels = GetSerializedGameObjects();
        foreach (var item in panels)
        {
            print(item.name);
        }
        eventSystem = EventSystem.current;
    }

    private void Update()
    {
        CheckMouseInput();
        CheckKeyboardInput();
        CheckGamepadInput();
    }

    public void OnPlayClicked()
    {
        ShowPlayPanel();
    }

    public void OnReturnFromGameCanvas()
    {
        
    }

    public void OnSettingsClicked()
    {
        ShowPanel(settingsPanel);
    }

    public void OnCreditsClicked()
    {
        print("credits");
    }

    public void OnQuitClicked()
    {
        Application.Quit();
    }

    public void OnTrainingClicked()
    {
        print("Training");
    }

    public void OnAchievementsClicked()
    {
        ShowPanel(achievementsPanel);
    }

    public void OnReturnClicked()
    {
        ShowPanel(mainPanel);
        mainPanelButtonClicked = null;
    }

    private void ShowPlayPanel()
    {
        SceneManager.LoadSceneAsync("Scenes/CharacterSelectScene");
    }

    private void ShowPanel(GameObject showPanel)
    {
        foreach (GameObject panel in panels) 
        {
            if (panel == showPanel)
            {
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }
        currentPanel = showPanel;
        if (!isUsingMouse)
        {
            if (mainPanelButtonClicked != null)
            {
                SetSelectedItem(mainPanelButtonClicked);
                mainPanelButtonClicked = null;
            }
            else
            {
                mainPanelButtonClicked = eventSystem.currentSelectedGameObject;
                SetSelectedItem(GetFirstItemOnPanel());
            }
        }
    }

    private void SetSelectedItem(GameObject gameObject)
    {
        eventSystem.SetSelectedGameObject(gameObject);
    }

    private GameObject GetFirstItemOnPanel()
    {
        for (int i = 0; i < currentPanel.transform.childCount; i++)
        {
            Transform child = currentPanel.transform.GetChild(i);
            Button button = child.GetComponent<Button>();

            // Check if the child has a Button component
            if (button != null)
            {
                return child.gameObject;
            }
        }
        return null;
    }

    private List<GameObject> GetSerializedGameObjects()
    {
        List<GameObject> serializedGameObjects = new List<GameObject>();

        Type type = GetType();
        FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        foreach (var field in fields)
        {
            if (Attribute.IsDefined(field, typeof(SerializeField)) && field.FieldType == typeof(GameObject))
            {
                GameObject serializedGameObject = field.GetValue(this) as GameObject;
                if (serializedGameObject != null)
                {
                    serializedGameObjects.Add(serializedGameObject);
                }
            }
        }

        return serializedGameObjects;
    }

    private void CheckMouseInput()
    {
        Vector2 mouseDelta = Mouse.current.delta.ReadValue();

        if (mouseDelta.x != 0 || mouseDelta.y != 0)
        {
            isUsingMouse = true;
            RemoveButtonSelected();
        }
    }

    private void CheckKeyboardInput()
    {
        if (Keyboard.current.anyKey.isPressed)
        {
            OnAnyKeyPressed();
        }
    }

    private void CheckGamepadInput()
    {
        Gamepad gamepad = Gamepad.current;

        if (gamepad != null)
        {
            foreach (InputControl control in gamepad.allControls)
            {
                if (control.IsPressed())
                {
                    OnAnyKeyPressed();
                }
            }
        }
    }

    private void OnAnyKeyPressed()
    {
        if (isUsingMouse) 
        {
            isUsingMouse = false;
            SetSelectedItem(GetFirstItemOnPanel());
        }
    }

    private void RemoveButtonSelected()
    {
        SetSelectedItem(null);
    }
}
