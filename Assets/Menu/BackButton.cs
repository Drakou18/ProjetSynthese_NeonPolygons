using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class BackButton : MonoBehaviour
{
    private InputSystemUIInputModule inputModule;
    private MainMenuManager mainMenuManager;
    // Start is called before the first frame update
    void Start()
    {
        inputModule = FindAnyObjectByType<InputSystemUIInputModule>();
        mainMenuManager = FindAnyObjectByType<MainMenuManager>();

        // Subscribe to the cancel action's performed event
        inputModule.cancel.action.performed += OnCancelPressed;
    }

    void OnCancelPressed(InputAction.CallbackContext context)
    {
        mainMenuManager.OnReturnClicked();
    }

    void OnDestroy()
    {
        inputModule.cancel.action.performed -= OnCancelPressed;
    }
}
