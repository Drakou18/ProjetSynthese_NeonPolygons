using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public Color team = new Color(255, 255, 255);
    public int color = 0;
    public int points = 0;
    public float damage = 1;

    public float GetKnockbackMult() {
        // Augmentation lin�aire du knockback. On pourrait faire une fonction log(n) ou nlog(n) � la place.
        return damage; 
    }
}
