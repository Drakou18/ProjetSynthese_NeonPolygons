using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityHandler : MonoBehaviour
{
    private Ability[] abilities;

    private void Awake()
    {
        this.abilities = GetComponents<Ability>();
    }

    public void DisableAllAbilities() {
        for (int i = 0; i < abilities.Length; i++) {
            abilities[i].enabled = false;
        }
    }

    public void ActivateAbilityByIndex(int index) {
        DisableAllAbilities();
        if (index < abilities.Length && index > -1)
        {
            abilities[index].enabled = true;
        }
    }

    public void ActivateAbilityByType(Type abilityType) {
        for (int i = 0; i < abilities.Length; i++) {
            if (abilities[i].GetType() == abilityType) {
                DisableAllAbilities();
                abilities[i].enabled = true;
                break;
            }
        }
    }

    public Ability[] GetAbilities() { 
        return abilities;
    }
}
