using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
	[SerializeField] private float speed = 5f;
	[SerializeField] private float groundCheckDistance = 1f;
	[SerializeField] private float jumpForce = 5;
    [SerializeField] private bool isGrounded = false;
	[SerializeField] private float squashAndStretchIntensity = 1;
	[SerializeField] private int maxJumps = 2;
    [SerializeField] private List<GameObject> weapons;
    [SerializeField] private float cancelMovementSpeedMult = 1.2f;
	[SerializeField] private float bounceFriction = .98f;

	private Camera cam;
	private int jumpsLeft = 2;
	private float minimumDamage = 0f;
    private SpriteRenderer spriteRenderer;
	private Collider2D playerCollider;
	private Rigidbody2D rb;
	private PlayerInput controls;
	private Color skinColor;
	private PlayerData playerData;
	private PlayerUI playerUI;
	private AbilityHandler abilityHandler;
	private bool isFrozen = false;
	private bool areInputsFrozen = false;
	private bool isTbaging = false;
	private bool isLanding = false;
	private int playerIndex = 0;
	private float horizontalInput;
	private Vector2 lookInput;
	private Vector2 lookLeftStickInput;
	private Vector2 lookRightStickInput;
	private Vector2 mousePosition;
	private bool isFiring = false;
    private Vector2 previousVelocity;

    public float defaultGravity = 1;
    public bool dead = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground) && Mathf.Abs(previousVelocity.x) > speed * cancelMovementSpeedMult)
        {
            Vector2 reflectionAxis = collision.contacts[0].normal;
            Vector2 result = Vector2.Reflect(previousVelocity, reflectionAxis);
            this.rb.velocity = result * bounceFriction;
        }

    }

    private void Start()
	{
		controls = GetComponentInParent<PlayerInput>();
		playerData = GetComponent<PlayerData>();
		abilityHandler = GetComponent<AbilityHandler>();

        controls.actions[Harmony.InputActions.Move].performed += ctx => horizontalInput = ctx.ReadValue<Vector2>()[0];
		controls.actions[Harmony.InputActions.Move].canceled += ctx => horizontalInput = 0;

		controls.actions[Harmony.InputActions.LookLeftStick].performed += ctx => lookLeftStickInput = ctx.ReadValue<Vector2>();

		controls.actions[Harmony.InputActions.LookRightStick].performed += ctx => lookRightStickInput = ctx.ReadValue<Vector2>();
		controls.actions[Harmony.InputActions.LookRightStick].canceled += ctx => lookRightStickInput = Vector2.zero;

		controls.actions[Harmony.InputActions.LookMouse].performed += ctx => mousePosition = ctx.ReadValue<Vector2>();

		controls.actions[Harmony.InputActions.Fire].performed += ctx => isFiring = true;
		controls.actions[Harmony.InputActions.Fire].canceled += ctx => isFiring = false;

		controls.actions[Harmony.InputActions.Tbag].performed += ctx => isTbaging = true;
		controls.actions[Harmony.InputActions.Tbag].canceled += ctx => isTbaging = false;

        controls.actions[Harmony.InputActions.Pause].performed += ctx => Pause();

		spriteRenderer = GetComponentInChildren<SpriteRenderer>();

		rb = GetComponent<Rigidbody2D>();
		playerCollider = GetComponent<Collider2D>();
		defaultGravity = rb.gravityScale;
		jumpsLeft = maxJumps;
		SetSkin(MultiplayerManager.Instance.GetNextAvailableSkin());
	}
	public void SetPlayerUI(PlayerUI ui)
	{
		playerUI = ui;
	}
	public Sprite GetSprite() {
		return spriteRenderer.sprite;
	}

	public AbilityHandler GetAbilityHandler() {
		return abilityHandler;
	}
	public bool IsPlayerFrozen() {
		return this.isFrozen;
	}

	public bool ArePlayerInputsFrozen()
	{
		return this.areInputsFrozen;
	}

	public void SetPlayerIndex(int playerIndex)
	{
		this.playerIndex = playerIndex;
	}

	public int GetPlayerIndex()
	{
		return this.playerIndex;
	}

	void Update()
	{
        previousVelocity = this.rb.velocity;
        lookInput = lookRightStickInput != Vector2.zero ? lookRightStickInput : lookLeftStickInput;
		if (mousePosition != Vector2.zero && cam) lookInput = (cam.ScreenToWorldPoint(mousePosition) - spriteRenderer.bounds.center).normalized;

		if (!isFrozen)
		{
			if (rb.gravityScale == 0)
				rb.gravityScale = defaultGravity;

            CheckGrounded();

            if (!isGrounded) spriteRenderer.transform.localScale = new(Mathf.Max(1f - rb.velocityY / (1000f / squashAndStretchIntensity), 0.5f), Mathf.Max(1f + rb.velocityY / (1000f / squashAndStretchIntensity), 0.5f));

            if (!areInputsFrozen)
			{
				if (Mathf.Abs(rb.velocityX) <= speed * cancelMovementSpeedMult)
				{
					rb.drag = 0;
					float move = horizontalInput;
					Vector2 moveDirection = new Vector2(move * speed, rb.velocity.y);
					rb.velocity = moveDirection;
				}
				else
				{
					rb.drag = 1;
				}

                if (isGrounded && !isLanding)
                {
                    if (isTbaging) spriteRenderer.transform.localScale = new(1, 0.5f);
                    else spriteRenderer.transform.localScale = Vector2.one;
                }

                if (controls.actions[Harmony.InputActions.Jump].triggered)
                    TryJump();
            }
			else
			{
                rb.velocity *= 0.98f;
            }
        }
		else
		{
			rb.velocity = Vector2.zero;
			rb.gravityScale = 0;
		}

		if (transform.position.y < -300f || transform.position.y > 600f || transform.position.x < -500 || transform.position.x > 500)
			Die();

	}
	public void SetDamage(float damage)
	{
		playerData.damage = damage;
	}
	public void Die()
	{
		if (playerUI)
        {
            dead = true;
            playerUI.Hide();
            gameObject.SetActive(false);
            MultiplayerManager.Instance.CheckForWinner();

        }
	}

	public void ShowInGameHUD()
	{
		playerUI.Show();
	}

	public void FreezePlayer()
	{
		this.isFrozen = true;
	}

	public void UnfreezePlayer()
	{
		this.isFrozen = false;
	}

	public void FreezeFor(float seconds)
	{
		StopAllCoroutines();
		StartCoroutine(FreezeCoroutine(seconds));
	}

	private IEnumerator FreezeCoroutine(float coolDown)
	{
		FreezePlayer();
		yield return new WaitForSeconds(coolDown);
		UnfreezePlayer();
	}

	public void FreezePlayerInputs()
	{
        this.areInputsFrozen = true;
    }

    public void UnfreezePlayerInputs()
    {
        this.areInputsFrozen = false;
    }

    public void TaseFor(float seconds)
    {
        FreezeInputsFor(seconds);
		// TODO: Taser animation
    }

	public void FreezeInputsFor(float seconds) {
        StopAllCoroutines();
        StartCoroutine(FreezeInputsCoroutine(seconds));
    }

    private IEnumerator FreezeInputsCoroutine(float coolDown)
    {
        FreezePlayerInputs();
        yield return new WaitForSeconds(coolDown);
        UnfreezePlayerInputs();
    }

    public void ApplyKnockback(Vector2 explosionPoint, float power, Vector2 extraForce) {
		Vector2 direction = (new Vector2(transform.position.x, transform.position.y) - explosionPoint + extraForce).normalized;
		float angle = Mathf.Atan2(direction.y, direction.x);
		angle = Mathf.MoveTowardsAngle(angle, 90f * Mathf.Deg2Rad, 20f * Mathf.Deg2Rad);
		direction = new(Mathf.Cos(angle), Mathf.Sin(angle));
		rb.AddForce(direction * power, ForceMode2D.Impulse);
	}

	public float KnockbackPowerToFreezeInputCooldown(float power) {
		float cooldown = power / 20000f;
		if (cooldown < 0.0f) {
			cooldown = 0f;
		}
		return cooldown;

	}

	public void Hurt(float amount) {
		if (playerData) {
			playerData.damage += amount;
		}
	}

	public void Heal(float amount)
	{
        if (playerData)
        {
            playerData.damage -= amount;

			if(playerData.damage < minimumDamage)
			{
				playerData.damage = minimumDamage;
            }
        }
    }

	public int GetPoints() {
		return playerData.points;
	}
	
	public void AddPoints(int amount) {
		playerData.points += amount;
	}

    private void CheckGrounded()
	{
        if (isGrounded)
        {
			isGrounded = IsGrounded();
			if (jumpsLeft <= 0) jumpsLeft = maxJumps;
        }
		else
		{
			isGrounded = IsGrounded();
			if (isGrounded)
			{
				StartCoroutine(Land(rb.velocityY));
			}
		}
    }

    private IEnumerator Land(float velocity)
    {
		isLanding = true;
		GameObject landParticles = PoolManager.Instance.Get(Harmony.PoolTags.land_particles);
		landParticles.transform.position = new(transform.position.x, spriteRenderer.bounds.min.y);
		landParticles.GetComponent<ParticleSystem>().startColor = skinColor;
		landParticles.SetActive(true);
		jumpsLeft = maxJumps;
		spriteRenderer.transform.localScale = new Vector2(
            Mathf.Max(1f - velocity / (500f / squashAndStretchIntensity), 0.3f),
            Mathf.Max(1f + velocity / (500f / squashAndStretchIntensity), 0.3f)
        );

        float duration = 0.1f;
        float elapsed = 0f;

        Vector2 startScale = spriteRenderer.transform.localScale;
        Vector2 targetScale = new Vector2(1, 1);

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float lerpFactor = elapsed / duration;
            spriteRenderer.transform.localScale = Vector2.Lerp(startScale, targetScale, lerpFactor);
            yield return null;
        }
		isLanding = false;
        spriteRenderer.transform.localScale = targetScale;
    }

	public void TryJump()
	{
		if (jumpsLeft > 0)
		{
			Jump();
			jumpsLeft--;
		}
	}
    public void Jump() {
        GameObject jumpParticles = PoolManager.Instance.Get(Harmony.PoolTags.double_jump_particles);
        jumpParticles.transform.position = new(transform.position.x, transform.position.y);
        jumpParticles.GetComponent<ParticleSystem>().startColor = skinColor;
        jumpParticles.SetActive(true);

        rb.velocityY = jumpForce;
    }

	public bool IsGrounded()
	{
		Vector2 boxCastPosition = new Vector2(transform.position.x, transform.position.y - 2.1f);
		Vector2 boxCastSize = new Vector2(playerCollider.bounds.size.x, 4f);
		float boxCastDistance = groundCheckDistance;
		RaycastHit2D hit = Physics2D.BoxCast(boxCastPosition, boxCastSize, 0, Vector2.down, boxCastDistance);
		return hit.collider != null && (hit.collider.CompareTag(Harmony.Tags.Ground) || hit.collider.CompareTag(Harmony.Tags.Player));
	}

	public GameObject getSelectedPlatform()
	{
        Vector2 boxCastPosition = new Vector2(transform.position.x, transform.position.y - 0.1f);
        Vector2 boxCastSize = new Vector2(playerCollider.bounds.size.x, 0.1f);
        float boxCastDistance = groundCheckDistance;
        RaycastHit2D hit = Physics2D.BoxCast(boxCastPosition, boxCastSize, 0, Vector2.down, boxCastDistance);
        if (hit.collider != null && hit.collider.CompareTag(Harmony.Tags.Ground))
		{
			return hit.collider.gameObject;
		}
		return null;
    }

	void OnDrawGizmos()
	{
		if (playerCollider != null)
		{
			Gizmos.color = Color.red;
			Vector3 boxCastPosition = transform.position + new Vector3(0f, -2.1f, 0f);
			Vector3 boxCastSize = new Vector3(playerCollider.bounds.size.x, 4f, 1f);
			Gizmos.DrawWireCube(boxCastPosition + Vector3.down * groundCheckDistance * 0.5f, boxCastSize);
		}
	}

	private void Pause()
	{
		PauseManager pauseManager = FindAnyObjectByType<PauseManager>();
        if(pauseManager)
        {
            pauseManager.TogglePause();
        }
    }

	public Vector2 GetLookInput() {
		return this.lookInput;
	}

	public bool isPlayerFiring() {
		return this.isFiring;
	}

	public Color GetTeam()
	{
		return playerData.team;
	}

	public void SetTeam(Color team)
	{
		playerData.team = team;
    }

	public void SetSkin(Sprite sprite)
	{
		spriteRenderer.sprite = sprite;
		skinColor = sprite.texture.GetPixel(0, 15);
	}

	public Color GetSkinColor()
	{
		return skinColor;
	}

	public void EquipRandomWeapon()
	{
		cam = Camera.main;
		StartCoroutine(EquipRandomWeaponCoroutine());
	}

	private IEnumerator EquipRandomWeaponCoroutine()
	{
		UnequipAllWeapons();

		yield return new WaitForSeconds(1);

        weapons[Random.Range(0, weapons.Count)].SetActive(true);
    }

	public void EquipeWeaponByType(System.Type type) {
        UnequipAllWeapons();
		GameObject weapon = weapons[0].transform.parent.GetComponentInChildren(type, true).gameObject;
		if (weapon)
		{
			weapon.SetActive(true);
		}
    }

	public void UnequipAllWeapons()
	{
        weapons.ForEach(weapon => weapon.SetActive(false));
    }

}
