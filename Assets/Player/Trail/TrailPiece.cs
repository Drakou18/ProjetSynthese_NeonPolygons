using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPiece : MonoBehaviour
{
    [SerializeField] private float lifeTime = 1;
    SpriteRenderer spriteRenderer;
    private float startingLifetime;

    void Start()
    {
        startingLifetime = lifeTime;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	private void OnEnable()
	{
        if(startingLifetime != 0) lifeTime = startingLifetime;
	}

	void Update()
    {
        lifeTime -= Time.deltaTime;
        spriteRenderer.color = new(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, (lifeTime / startingLifetime) / 4f);
        if (lifeTime <= 0) PoolManager.Instance.ReturnToPool(Harmony.PoolTags.trail, gameObject);
    }
}
