using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4 : Holdable
{
    [SerializeField] protected float bulletCooldown = 0.1f;
    [SerializeField] protected string bulletType;
    [SerializeField] protected float bulletSpawnOffset = 2;
    [SerializeField] private LayerMask terrainMask;
    [SerializeField] private float C4HoldCooldown = 1.5f;
    [SerializeField] private float explosionDelay = .1f;
    [SerializeField] private float throwSpeed = 1000f;

    private bool preventNextShoot = false;
    protected List<C4Projectile> C4s = new List<C4Projectile>();

    private bool wasFiring = false;
    protected float cooldownLeft = 0f;

    protected override void Awake()
    {
        this.cooldownLeft = 0;
        this.C4s = new List<C4Projectile>();
        base.Awake();
    }

    protected void OnEnable()
    {
        this.C4s = new List<C4Projectile>();
    }

    protected void tryShoot() {
        if (cooldownLeft <= 0f)
        {
            cooldownLeft = bulletCooldown;
            shoot();
        }
    }

    protected void shoot() {
        GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);
        bulletToShoot.SetActive(true);
        Color team = new Color(1f, 1f, 1f);
        if (player)
        {
            PlayerData data = player.GetComponent<PlayerData>();
            if (data)
                team = data.team;
        }

        Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        RaycastHit2D hit = Physics2D.Raycast(transform.position - (Vector3)(direction * offsetFromCenter), direction, offsetFromCenter + bulletSpawnOffset, terrainMask);

        if (hit)
        {
            bulletToShoot.transform.position = hit.point;
        }
        else
        {
            bulletToShoot.transform.position = transform.position + new Vector3(
                bulletSpawnOffset * Mathf.Cos(rotation * Mathf.Deg2Rad),
                bulletSpawnOffset * Mathf.Sin(rotation * Mathf.Deg2Rad),
                0
            );
        }

        C4Projectile c4 = bulletToShoot.GetComponent<C4Projectile>();
        if (c4)
        {
            c4.SetTeam(team);
            Vector2 throwDirection = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
            c4.setKinematicState(false);
            c4.setVelocity(throwDirection * throwSpeed);
            this.C4s.Add(c4);
        }
        bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, rotation * Mathf.Deg2Rad);
    }

    protected override void Update()
    {
        if (cooldownLeft > 0f)
            cooldownLeft -= Time.deltaTime;

        if (!player.isPlayerFiring() && wasFiring) {

            if (!preventNextShoot)
            {
                tryShoot();
            }
            else {
                preventNextShoot = false;
            }
            tryShoot();
        }

        if (player.isPlayerFiring() &&  !wasFiring) {
            StopCoroutine(activateC4());
            StartCoroutine(activateC4());
        }

        wasFiring = player.isPlayerFiring();
        base.Update();
    }

    protected IEnumerator activateC4() {
        float cooldown = C4HoldCooldown;
        while (player.isPlayerFiring() && cooldown > 0f)
        {
            cooldown -= Time.deltaTime;
            yield return null;
        }

        if (player.isPlayerFiring()) {
            preventNextShoot = true;
            StartCoroutine(activateAllC4());
        }
    }

    protected IEnumerator activateAllC4() {
        for (int i = 0; i < C4s.Count; i++) {
            if (C4s[i] && C4s[i].enabled) {
                C4s[i].Explode(new Vector2(0, 0));
                yield return new WaitForSeconds(explosionDelay);
            }
        }
        this.C4s = new List<C4Projectile>();
    }
}
