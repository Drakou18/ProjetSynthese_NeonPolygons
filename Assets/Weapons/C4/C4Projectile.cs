using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4Projectile : MonoBehaviour
{
    [SerializeField] private float explosionRadius = 1;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] float explosionPower = 1f;
    [SerializeField] float explosionDamage = 1f;
    [SerializeField] Color team;

    private Rigidbody2D rb;
    private GameObject platform;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground)) {
            this.platform = collision.gameObject;
        }
        else if (collision.gameObject.CompareTag(Harmony.Tags.DestroyArea)) {
            Explode(new Vector2(0, 0));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground) && collision.gameObject == platform.gameObject)
        {
            this.platform = null;
        }
    }

    public void setVelocity(Vector2 velocity)
    {
        if (rb)
            rb.velocity = velocity;
    }

    public void setKinematicState(bool isKinematic)
    {
        if (rb)
        {
            if (isKinematic) {
                rb.velocity = new Vector2(0, 0);
            }
            rb.isKinematic = isKinematic;
        }
    }

    private void Awake()
    {
        this.rb = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        if (rb)
            setKinematicState(platform);
    }

    public void SetTeam(Color team)
    {
        this.team = team;
    }

    public Color GetTeam()
    {
        return team;
    }

    public void Explode(Vector2 extraForce)
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(this.team);
        explosion.Explode(explosionRadius, extraForce);
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.c4, this.gameObject);
    }
}
