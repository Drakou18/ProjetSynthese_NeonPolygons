using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceBullet : MonoBehaviour
{
    private float fuseTimeLeft;
    private float explosionRadius;
    private float explosionPower;
    private float explosionDamage;
    private Color team;
    private Vector2 previousVelocity;
    private Rigidbody2D rb;
    [SerializeField] private float bounceFriction = .9f;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground))
        {
            Vector2 reflectionAxis = collision.contacts[0].normal;
            Vector2 result = Vector2.Reflect(previousVelocity, reflectionAxis);
            this.rb.velocity = result * bounceFriction;
        }
        else if (collision.gameObject.CompareTag(Harmony.Tags.Player))
        {
            PlayerData playerData = collision.gameObject.GetComponent<PlayerData>();
            if (playerData && playerData.team != this.team)
            {
                Explode();
            }
        }

    }

    public void SetTeam(Color color)
    {
        team = color;
    }
    public void SetFuse(float fuseTime)
    {
        fuseTimeLeft = fuseTime;
    }
    public void SetExplosionRadius(float radius)
    {
        explosionRadius = radius;
    }

    public void SetExplosionPower(float power)
    {
        explosionPower = power;
    }

    public void SetExplosionDamage(float damage)
    {
        explosionDamage = damage;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (rb) {
            previousVelocity = rb.velocity;
        }

        fuseTimeLeft -= Time.deltaTime;
        if (fuseTimeLeft < 0)
        {
            Explode();
        }
    }

    private void Explode()
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();

        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(team);

        explosion.Explode(explosionRadius, new Vector2(0, 0));
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.bounce_bullet, gameObject);
    }
}
