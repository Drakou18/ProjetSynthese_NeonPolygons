using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour
{
	[SerializeField] SpriteRenderer spriteRenderer;
	[SerializeField] BoxCollider2D boxCollider;

	private float defaultScale = 0.2f;
	private float damageInterval;
	private float timeUntilNextDamage;
	private Color team;

	private void Start()
	{
		defaultScale = transform.localScale.x;
		spriteRenderer = GetComponent<SpriteRenderer>();
		boxCollider = GetComponent<BoxCollider2D>();
	}

	private void OnEnable()
	{
		spriteRenderer.color = new(0, 0, 0, 0);
		boxCollider.enabled = false;
	}

	public void ActivatePreviewFade(float duration, float startAlpha, float endAlpha, float startScaleModifier, float endScaleModifier)
	{
		StopAllCoroutines();
		StartCoroutine(PreviewFade(duration, startAlpha, endAlpha, startScaleModifier, endScaleModifier));
	}
	
	private IEnumerator PreviewFade(float duration, float startAlpha, float endAlpha, float startScaleModifier, float endScaleModifier)
	{
		float time = 0;
		
		while (time < duration)
		{
			time += Time.deltaTime;

			float normalizedTime = time / duration;
			spriteRenderer.color = new(1, 1, 1, Mathf.Lerp(startAlpha, endAlpha, normalizedTime));
			transform.localScale = Mathf.Lerp(defaultScale + startScaleModifier, defaultScale + endScaleModifier, normalizedTime) * Vector2.one;
			yield return null;
		}
	}

	public void Activate(float damageInterval, Color team) 
	{
		this.team = team;
		this.damageInterval = damageInterval;
		timeUntilNextDamage = this.damageInterval;
		StopAllCoroutines();
		transform.localScale = (defaultScale + 0.05f) * Vector2.one; 
		boxCollider.enabled = true;
		spriteRenderer.color = Color.white;
	}

	public void Deactivate()
	{
		StopAllCoroutines();
		boxCollider.enabled = false;
	}

    private void Update()
    {
        timeUntilNextDamage -= Time.deltaTime;

		if (timeUntilNextDamage < 0 )
		{
			timeUntilNextDamage = damageInterval;
			if (boxCollider.isActiveAndEnabled)
			{
				List<Collider2D> colliders = new();
				boxCollider.Overlap(colliders);

				colliders.ForEach(collider =>
				{
					if (collider.CompareTag(Harmony.Tags.Player))
					{
						Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
						explosion.transform.position = collider.transform.position;
						explosion.gameObject.SetActive(true);
						explosion.SetTeam(team);
						explosion.SetExplosionDamage(0.15f);
						explosion.Explode(20, new Vector2(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad)) * 100f);
					}
				});
			}
		}
    }
}
