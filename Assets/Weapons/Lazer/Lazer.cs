using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : Gun
{
    [SerializeField] private LazerBeam lazerBeam;
	[SerializeField] private float startingFadeLenght = 3;
	[SerializeField] private float activeLazerDuration = 1;
	[SerializeField] private float endingFadeLenght = 1;
	[SerializeField] private float damageInterval = 0.1f;

	protected override void shoot()
	{
		StartCoroutine(activateLazer());
	}

	private IEnumerator activateLazer()
	{
		player.FreezeFor(startingFadeLenght + activeLazerDuration + endingFadeLenght);
		lazerBeam.ActivatePreviewFade(startingFadeLenght, 0, 1, 0, 0);
		yield return new WaitForSeconds(startingFadeLenght);
		lazerBeam.Activate(damageInterval, player.GetTeam());
		yield return new WaitForSeconds(activeLazerDuration);
		lazerBeam.Deactivate();
		lazerBeam.ActivatePreviewFade(endingFadeLenght, 1, 0, 0.05f, 0);
	}
}
