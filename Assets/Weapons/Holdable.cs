using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Holdable : MonoBehaviour
{
    [SerializeField] protected float spinSpeed = .2f;
    [SerializeField] protected float offsetFromCenter = 8f;
    protected Player player;
    protected SpriteRenderer playerSpriteRenderer;
    protected float rotation = 0f;

    protected virtual void Awake()
    {
        this.player = null;
        if (transform.parent != null && transform.parent.GetComponentInParent<Player>()) 
        {
			this.player = transform.parent.GetComponentInParent<Player>();
            playerSpriteRenderer = player.GetComponentInChildren<SpriteRenderer>();
		}
            
    }


    protected void updateRotationAndPosition(float goalRotation)
    {
        float newRotation = Mathf.LerpAngle(this.rotation, goalRotation, spinSpeed);

        // Calcule pour �viter que le modulo donne un chiffre n�gatif (-270 % 360 = -270)
        newRotation = (newRotation % 360 + 360) % 360;
        rotation = newRotation;

        transform.rotation = Quaternion.Euler(0, 0, newRotation);

        Vector3 localScale = this.transform.localScale;
        localScale.y = Mathf.Abs(localScale.y);
        if (newRotation > 90f && newRotation < 270f)
        {
            localScale.y *= -1;
        }

        transform.localScale = localScale;

        transform.position = playerSpriteRenderer.bounds.center + new Vector3(
            offsetFromCenter * Mathf.Cos(newRotation * Mathf.Deg2Rad),
            offsetFromCenter * Mathf.Sin(newRotation * Mathf.Deg2Rad),
            0
        );
    }

    protected virtual void Update() {
        if (player && !player.IsPlayerFrozen())
        {
            Vector2 lookDirection = player.GetLookInput();
            float angle = Mathf.Atan2(lookDirection.y, lookDirection.x);
            updateRotationAndPosition(angle * Mathf.Rad2Deg);
        }
    }
}
