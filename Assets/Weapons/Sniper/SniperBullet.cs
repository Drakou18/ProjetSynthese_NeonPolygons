using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperBullet : Bullet
{
    private CapsuleCollider2D capsuleCollider;

    private void Start() {
        this.capsuleCollider = GetComponent<CapsuleCollider2D>();
        capsuleCollider.direction = CapsuleDirection2D.Horizontal;
        capsuleCollider.size = new Vector2(1, 1);
        capsuleCollider.offset = new Vector2(0, 0);
    }

    protected override void Update()
    {
        // On ralonge la hitbox du sniperbullet vers l'arri�re pour creuser une ligne droite sans trous.
        capsuleCollider.size = new Vector2(Mathf.Max(0, speed * Time.deltaTime), 1f);
        capsuleCollider.offset = new Vector2(-(speed * Time.deltaTime - 1f) / 2f, 0);
        base.Update();
    }
}
