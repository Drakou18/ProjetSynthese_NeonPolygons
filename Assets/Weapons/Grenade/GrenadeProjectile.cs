using UnityEngine;

public class GrenadeProjectile : MonoBehaviour
{
	private float fuseTimeLeft = 10f;
	private float explosionRadius = 10f;
	private float explosionPower = 1000;
	private float explosionDamage = 0.1f;
	private Color team;

	public void SetTeam(Color color)
	{
		team = color;
	}

	public Color GetTeam()
	{ 
		return team; 
	}
	public void SetFuse(float fuseTime)
	{
		fuseTimeLeft = fuseTime;
	}
	public void SetExplosionRadius(float radius)
	{
		explosionRadius = radius;
	}

	public void SetExplosionPower(float power)
	{
		explosionPower = power;
	}

	public void SetExplosionDamage(float damage)
	{
		explosionDamage = damage;
	}

	private void Update()
	{
		fuseTimeLeft -= Time.deltaTime;
		if (fuseTimeLeft < 0)
		{
			Explode();
		}
	}

	private void Explode()
	{
		Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();

		explosion.gameObject.SetActive(true);
		explosion.transform.position = transform.position;
		explosion.SetExplosionPower(explosionPower);
		explosion.SetExplosionDamage(explosionDamage);
		explosion.SetTeam(team);

		explosion.Explode(explosionRadius, new Vector2(0, 0));
		PoolManager.Instance.ReturnToPool(Harmony.PoolTags.grenade, gameObject);
	}
}
