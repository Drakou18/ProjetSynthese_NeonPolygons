using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Gun
{
	[SerializeField] private float throwingStrength = 200;
	[SerializeField] private float fuseTime = 3f;
	[SerializeField] private float explosionRadius = 10f;
	[SerializeField] private float explosionPower = 1000;
	[SerializeField] private float explosionDamage = 0.1f;
	protected override void shoot()
	{
		GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);
		Color team = new Color(255, 255, 255);
		if (player != null)
		{
			PlayerData data = player.GetComponent<PlayerData>();
			if (data)
				team = data.team;
		}

		bulletToShoot.transform.position = transform.position + new Vector3(
			bulletSpawnOffset * Mathf.Cos(rotation * Mathf.Deg2Rad),
			bulletSpawnOffset * Mathf.Sin(rotation * Mathf.Deg2Rad),
			0
		);
		Bullet bullet = bulletToShoot.GetComponent<Bullet>();
		if (bullet) bullet.SetTeam(team);
		bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, rotation * Mathf.Deg2Rad);
		Vector2 shootDirection = new Vector2(
			Mathf.Cos(rotation * Mathf.Deg2Rad),
			Mathf.Sin(rotation * Mathf.Deg2Rad)
		);
		GrenadeProjectile grenadeProjectile = bulletToShoot.GetComponent<GrenadeProjectile>();
		grenadeProjectile.SetFuse(fuseTime);
		grenadeProjectile.SetExplosionRadius(explosionRadius);
		grenadeProjectile.SetExplosionPower(explosionPower);
		grenadeProjectile.SetExplosionDamage(explosionDamage);
		grenadeProjectile.SetTeam(team);
		bulletToShoot.SetActive(true);
		bulletToShoot.GetComponent<Rigidbody2D>().velocity = shootDirection * throwingStrength;
	}
}
