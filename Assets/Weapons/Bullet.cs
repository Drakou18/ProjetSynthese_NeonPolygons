using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem.Controls;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected float speed = 100;

    [SerializeField] private string poolTag;
    [SerializeField] private float totalLifetime = 10;
    [SerializeField] private bool explodeOnDestroy = false;
    [SerializeField] private float destroyAfterHitDelay = 0f;
    [SerializeField] private float explosionRadius = 1;
    [SerializeField] private LayerMask targetLayer;
    [SerializeField] private float bulletVelocityKnockbackPowerMult = .1f; // � quel point le knockback devrait prendre en compte la vitesse de la bullet

    [SerializeField] public float explosionPower = 1f;
    [SerializeField] public float explosionDamage = 1;
    [SerializeField] public float damage = 1;
    [SerializeField] private Color team;

    private SpriteRenderer spriteRenderer;

    private float currentLifeTime = 1;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void Update()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad)), speed * Time.deltaTime, targetLayer);
        bool preventMove = false;
        Vector2 extraForce = new Vector2(0, 0);

        if (hits.Length > 0) {
            for (int i = 0; i < hits.Length; i++) {
                
                Player player = hits[i].collider.gameObject.GetComponent<Player>();
                if (player)
                {
                    if (player.GetTeam() != this.team)
                    {
                        transform.position = hits[i].point;
                        //Extra force
                        extraForce = new Vector2(speed * bulletVelocityKnockbackPowerMult * Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad), speed * bulletVelocityKnockbackPowerMult * Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad));
                        currentLifeTime = 0;
                        player.Hurt(this.damage);
                        preventMove = true;
                        break;
                    }
                }
                else if (explodeOnDestroy && currentLifeTime > destroyAfterHitDelay) {
                    transform.position = hits[i].point;
                    currentLifeTime = destroyAfterHitDelay;
                    preventMove = true;
                    break;
                }
            }
        }

        if (!preventMove)
            Move(); 

        currentLifeTime -= Time.deltaTime;
        if (currentLifeTime <= 0)
        {
            if (explodeOnDestroy) Explode(extraForce);
            PoolManager.Instance.ReturnToPool(poolTag, gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad)) * speed * Time.deltaTime);
    }

    public void Explode(Vector2 extraForce)
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(this.team);
        explosion.Explode(explosionRadius, extraForce);
    }

    public void SetTeam(Color team)
    {
        this.team = team;
        spriteRenderer.color = team;
    }

	public Color GetTeam()
    {
        return this.team;
    }

    public string GetPoolTag()
    { 
        return this.poolTag; 
    }    

    protected void OnEnable()
    {
        currentLifeTime = totalLifetime;
    }

    protected void Move() 
    {
        transform.position += speed * Time.deltaTime * new Vector3(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad), 0);
    }
}
