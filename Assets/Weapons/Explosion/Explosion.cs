using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class Explosion : MonoBehaviour
{
	private CircleCollider2D collider2d;
	private float explosionPower = 1000f;
	private float explosionDamage = 0.1f;
	private Color team = new Color(1f, 1f, 1f);
	private Vector2 extraForce;
	private ParticleSystem ps;

    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.CompareTag(Harmony.Tags.Player)) {
            Player player = collision.GetComponent<Player>();
            PlayerData playerData = collision.GetComponent<PlayerData>();


            if (player && playerData) {
				if (playerData.team != this.team)
					player.Hurt(explosionDamage);

                player.ApplyKnockback(this.transform.position, explosionPower * playerData.GetKnockbackMult(), extraForce);
			}
		}
    }

    public void SetExplosionPower(float explosionPower) {
		this.explosionPower = explosionPower;
	}

	public void SetExplosionDamage(float explosionDamage) {
		this.explosionDamage = explosionDamage;
	}

	public void SetTeam(Color team) {
		this.team = team;
		ps.startColor = team;
		
	}
	
	private void Awake()
    {
        collider2d = gameObject.GetComponent<CircleCollider2D>();
		ps = GetComponent<ParticleSystem>();
    }
    public void Explode(float size, Vector2 extraForce)
	{
		this.extraForce = extraForce;
        transform.localScale = size * Vector2.one;
		StopAllCoroutines();
		StartCoroutine(ExplodeCoroutine());
	}

    public IEnumerator ExplodeCoroutine()
	{
		collider2d.enabled = true;
		yield return new WaitForSeconds(0.1f);
        collider2d.enabled = false;
		yield return new WaitForSeconds(0.3f);
		PoolManager.Instance.ReturnToPool(Harmony.PoolTags.explosion, gameObject);
	}
}
