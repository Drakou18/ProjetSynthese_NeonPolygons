using Harmony;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{ 
    private bool isPausing = false;
    private bool isUsingMouse = true;
    private EventSystem eventSystem;
    private GameObject currentPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject countdownPanel;

    private void Start()
    {
        eventSystem = EventSystem.current;
    }

    private void Update()
    {
        if (isPausing)
        {
            CheckMouseInput();
            CheckKeyboardInput();
            CheckGamepadInput();
        }
    }

    public void TogglePause()
    {
        if (isPausing) OnResumeClicked();
        else
        {
            currentPanel = mainPanel;
            isPausing = true;
            SetPlayerActivation();
            TogglePanel();
        }
    }

    private void TogglePanel()
    {
        pausePanel.SetActive(isPausing);
        if (isPausing ) Time.timeScale = 0;
        else Time.timeScale = 1;
    }

    public void OnResumeClicked()
    {
        StartCoroutine(Countdown());
    }

    public void OnSettingsClicked()
    {
        ChangePanel(settingsPanel);
    }

    public void OnQuitClicked()
    {
        SceneManager.LoadSceneAsync("Scenes/CharacterSelectScene");
    }

    public void onBackClicked()
    {
        ChangePanel(mainPanel);
    }

    private IEnumerator Countdown()
    {
        pausePanel.gameObject.SetActive(false);
        countdownPanel.gameObject.SetActive(true);
        int timeLeft = 3;

        while (timeLeft > 0)
        {
            countdownPanel.GetComponentInChildren<TextMeshProUGUI>().text = timeLeft.ToString();
            yield return new WaitForSecondsRealtime(1);
            timeLeft--;
        }
        Time.timeScale = 1;
        countdownPanel.gameObject.SetActive(false);
        Unpause();
    }

    private void SetPlayerActivation()
    {
        Player[] players = FindObjectsByType<Player>(FindObjectsSortMode.None);
        foreach (var p in players)
        {
            if (isPausing) p.FreezePlayer();
            else p.UnfreezePlayer();
        }
    }

    private void Unpause()
    {
        isPausing = false;
        isUsingMouse = true;
        SetPlayerActivation();
    }

    private void CheckMouseInput()
    {
        Vector2 mouseDelta = Mouse.current.delta.ReadValue();

        if (mouseDelta.x != 0 || mouseDelta.y != 0)
        {
            isUsingMouse = true;
            SetSelectedItem(null);
        }
    }

    private void CheckKeyboardInput()
    {
        if (Keyboard.current.anyKey.isPressed)
        {
            OnAnyKeyPressed();
        }
    }

    private void CheckGamepadInput()
    {
        Gamepad gamepad = Gamepad.current;

        if (gamepad != null)
        {
            foreach (InputControl control in gamepad.allControls)
            {
                if (control.IsPressed())
                {
                    OnAnyKeyPressed();
                }
            }
        }
    }

    private void OnAnyKeyPressed()
    {
        if (isUsingMouse)
        {
            isUsingMouse = false;
            SetSelectedItem(GetFirstItemOnPanel());
        }
    }

    private GameObject GetFirstItemOnPanel()
    {
        for (int i = 0; i < currentPanel.transform.childCount; i++)
        {
            Transform child = currentPanel.transform.GetChild(i);
            Button button = child.GetComponent<Button>();

            // Check if the child has a Button component
            if (button != null)
            {
                return child.gameObject;
            }
        }
        return null;
    }

    private void SetSelectedItem(GameObject gameObject)
    {
        eventSystem.SetSelectedGameObject(gameObject);
    }

    private void ChangePanel(GameObject panel)
    {
        currentPanel.SetActive(false);
        currentPanel = panel;
        currentPanel.SetActive(true);
        if (!isUsingMouse)
        {
            SetSelectedItem(GetFirstItemOnPanel());
        }
    }
}