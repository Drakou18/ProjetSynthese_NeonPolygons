using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI multText;
    [SerializeField] private CooldownUI cooldownUI;
    [SerializeField] private Image playerImageUI;
    [SerializeField] private Image backgroundTeamUI;
    private Player player;

    void Start()
    {
        
    }

    public Player GetPlayer() {
        return this.player;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void UpdatePlayerUI()
    {
        if (player) {
            // Image du joueur
            Sprite sprite = player.GetSprite();
            playerImageUI.sprite = sprite;

            // Background
            Color team = player.GetTeam();
            backgroundTeamUI.color = new Color(team.r, team.g, team.b, backgroundTeamUI.color.a);

            // Multiplicateur
            PlayerData playerData = player.gameObject.GetComponent<PlayerData>();
            if (playerData) {
                multText.text = "x" + playerData.damage.ToString("0.0");
            }

            // Cooldown
            Ability[] abilities = player.gameObject.GetComponents<Ability>();
            Ability enabledAbility = null;

            for (int i = 0; i < abilities.Length; i++)
            {
                if (abilities[i].enabled) {
                    enabledAbility = abilities[i];
                    break;
                }
            }

            if (enabledAbility)
                cooldownUI.UpdateCooldown(enabledAbility.GetCooldownRatio());
        }
    }

    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine(HideCoroutine());
    }

    public void Show()
    {
        StopAllCoroutines();
        StartCoroutine(ShowCoroutine());
    }

    private IEnumerator HideCoroutine()
    {
        while (transform.position.y > -120)
        {
            transform.position = new(transform.position.x, transform.position.y - Time.deltaTime * 600);
			yield return null;
		}   
    }

    private IEnumerator ShowCoroutine()
    {
        while (transform.position.y < 120)
        {
            transform.position = new(transform.position.x, transform.position.y + Time.deltaTime * 600);
            yield return null;
        }
    }
}
