using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownUI : MonoBehaviour
{
    [SerializeField] private Image backgroundImageGameObject;
    [SerializeField] private Image overlayImageGameObject;

    [SerializeField] Sprite backgroundSprite;
    [SerializeField] Sprite overlaySprite;

    [SerializeField] private Color backgroundColor = new Color(1f, 0f, 0f, 1f);
    [SerializeField] private Color overlayColor = new Color(0.5f, 0, 0, 1f);

    private Player player;

    private void Start()
    {
        backgroundImageGameObject.sprite = backgroundSprite;
        overlayImageGameObject.sprite = overlaySprite;

        overlayImageGameObject.type = Image.Type.Filled;

        backgroundImageGameObject.color = backgroundColor;
        overlayImageGameObject.color = overlayColor;
    }

    public void UpdateCooldown(float value)
    {
        overlayImageGameObject.fillAmount = value;
    }

}