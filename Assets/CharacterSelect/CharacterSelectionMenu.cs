using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class CharacterSelectionMenu : MonoBehaviour
{
    [SerializeField] private SpriteRenderer playerSprite;
    [SerializeField] private Player player;

    private bool ready = false;

    private LinkedList<Color> teams = new LinkedList<Color>();

    PlayerInput playerInput;
    private MultiplayerManager multiplayerManager;
    private GameObject parent;
    private AbilitySelection currentAbility;
    private float defaultAbilityScale;
    private LinkedList<AbilitySelection> abilities = new();
    private Vector2 defaultPosition;
    private Color currentTeam;
    private RawImage background;
    private GameObject colorButton;
    private GameObject colorLeft;
    private GameObject colorRight;
    private Image abilityImage;
    private GameObject abilityLeft;
    private GameObject abilityRight;
    private GameObject teamButton;
    private GameObject teamLeft;
    private GameObject teamRight;
    private GameObject abilityBackground;
    private SpriteRenderer readyForeground;
    private TextMeshProUGUI readyText;
    private bool acceptInputs = false;

    private GameObject cursorLeft;
    private GameObject cursorRight;
    private float cursorLeftX;
    private float cursorRightX;

    private MultiplayerEventSystem eventSystem;
    private Vector2 defaultPlayerPos;

    private GameObject previousSelectedButton;

    private void Awake()
    {
        multiplayerManager = MultiplayerManager.Instance;
        playerInput = GetComponentInParent<PlayerInput>();
        parent = transform.parent.gameObject;
        eventSystem = GetComponent<MultiplayerEventSystem>();
        background = transform.Find(Harmony.GameObjects.Background).GetComponent<RawImage>();
        background.material = new(background.material);
		colorButton = transform.Find(Harmony.GameObjects.Color).gameObject;
		colorLeft = transform.Find(Harmony.GameObjects.ColorLeft).gameObject;
        colorRight = transform.Find(Harmony.GameObjects.ColorRight).gameObject;
        abilityBackground = transform.Find(Harmony.GameObjects.AbilityBackground).gameObject;
		abilityImage = abilityBackground.GetComponentInChildren<Image>();
        abilityLeft = transform.Find(Harmony.GameObjects.AbilityLeft).gameObject;
        abilityRight = transform.Find(Harmony.GameObjects.AbilityRight).gameObject;
        teamButton = transform.Find(Harmony.GameObjects.Team).gameObject;
        teamLeft = transform.Find(Harmony.GameObjects.TeamLeft).gameObject;
        teamRight = transform.Find(Harmony.GameObjects.TeamRight).gameObject;
        cursorLeft = transform.Find(Harmony.GameObjects.CursorLeft).gameObject;
        cursorRight = transform.Find(Harmony.GameObjects.CursorRight).gameObject;
        cursorRightX = cursorRight.transform.localPosition.x;
        cursorLeftX = cursorLeft.transform.localPosition.x;
        readyForeground = transform.Find(Harmony.GameObjects.ReadyForeground).GetComponent<SpriteRenderer>();
        readyText = readyForeground.transform.Find(Harmony.GameObjects.Canvas).GetComponentInChildren<TextMeshProUGUI>();
        player.SetPlayerIndex(playerInput.playerIndex);

    }
    private void Start()
    {
        defaultPlayerPos = player.gameObject.transform.position;
        multiplayerManager.GetTeamColors().ForEach(color => { teams.AddFirst(color); });
        player.SetTeam(multiplayerManager.GetTeam(0));
        SetTeam(multiplayerManager.GetTeam(playerInput.playerIndex));
        defaultPosition = transform.position;
        multiplayerManager.GetAbilities().ForEach(ability => abilities.AddFirst(ability));
        defaultAbilityScale = abilityBackground.transform.localScale.x;
        readyForeground.gameObject.SetActive(false);
		NextAbility();
		Invoke("ActivateAcceptInputs", 0.3f);
        multiplayerManager.CheckAllPlayersReady();
    }

    private void Update()
    {
        if (previousSelectedButton != eventSystem.currentSelectedGameObject && !ready)
        {
            if (!eventSystem.currentSelectedGameObject.name.Contains("Left") && !eventSystem.currentSelectedGameObject.name.Contains("Right"))
            {
                cursorLeft.transform.position = new(cursorLeft.transform.position.x, eventSystem.currentSelectedGameObject.transform.position.y);
                cursorRight.transform.position = new(cursorRight.transform.position.x, eventSystem.currentSelectedGameObject.transform.position.y);
                previousSelectedButton = eventSystem.currentSelectedGameObject;
                StopAllCoroutines();
                StartCoroutine(MoveRightCursor());
                StartCoroutine(MoveLeftCursor());
            }
            else
            {
                if (eventSystem.currentSelectedGameObject == colorRight) NextSkin();
                else if (eventSystem.currentSelectedGameObject == colorLeft) PreviousSkin();
                else if (eventSystem.currentSelectedGameObject == teamLeft) PreviousTeam();
                else if (eventSystem.currentSelectedGameObject == teamRight) NextTeam();
                else if (eventSystem.currentSelectedGameObject == abilityLeft) PreviousAbility();
                else if (eventSystem.currentSelectedGameObject == abilityRight) NextAbility();
            }

        }
        Vector2 targetPosition = new(defaultPosition.x + Mathf.Cos(Time.time + multiplayerManager.GetTeamNumber(currentTeam) - 4) * 2, defaultPosition.y + Mathf.Sin(Time.time + multiplayerManager.GetTeamNumber(currentTeam)) * 2);
        transform.position = new(Mathf.MoveTowards(transform.position.x, targetPosition.x, Time.deltaTime), Mathf.MoveTowards(transform.position.y, targetPosition.y, Time.deltaTime));
        abilityBackground.transform.localScale = Vector2.one * Mathf.MoveTowards(abilityBackground.transform.localScale.x, defaultAbilityScale, Time.deltaTime * 2);
        if (readyText.isActiveAndEnabled) readyText.fontSize = Mathf.MoveTowards(readyText.fontSize, 1.5f, Time.deltaTime * 10);
    }
    public void NextSkin()
    {
        multiplayerManager.AddSkinToBack(playerSprite.sprite);
        player.SetSkin(multiplayerManager.GetNextAvailableSkin());
        eventSystem.SetSelectedGameObject(colorButton);
        BouncePlayer();
        StopAllCoroutines();
        StartCoroutine(MoveRightCursor());
    }

	public void PreviousSkin()
	{
        multiplayerManager.AddSkinToFront(playerSprite.sprite);
		player.SetSkin(multiplayerManager.GetPreviousAvailableSkin());
		eventSystem.SetSelectedGameObject(colorButton);
        BouncePlayer();
        StopAllCoroutines();
        StartCoroutine(MoveLeftCursor());
    }
    public void SetTeam(Color team)
    {
        int index = 0;
        while (index < 10)
        {
            index++;
            NextTeam();
            if (currentTeam == team) return;
        }
        Debug.LogWarning("Team doesn't exist :" + team);
    }
    public void NextTeam()
    {
		eventSystem.SetSelectedGameObject(teamButton);
		Color team = teams.Last.Value;
		teams.RemoveLast();
		if (!teams.Contains(player.GetTeam())) teams.AddFirst(player.GetTeam());
		teamButton.GetComponentInChildren<TextMeshProUGUI>().text = "Team " + multiplayerManager.GetTeamNumber(team);
        player.SetTeam(team);
		background.material.SetColor("_SolidOutline", team);
        currentTeam = team;
		StopAllCoroutines();
		StartCoroutine(MoveRightCursor());
	}

    public void PreviousTeam()
    {
		eventSystem.SetSelectedGameObject(teamButton);
		Color team = teams.First.Value;
		teams.RemoveFirst();
		if (!teams.Contains(player.GetTeam())) teams.AddLast(player.GetTeam());
		teamButton.GetComponentInChildren<TextMeshProUGUI>().text = "Team " + multiplayerManager.GetTeamNumber(team);
		player.SetTeam(team);
		background.material.SetColor("_SolidOutline", team);
        currentTeam = team;
		StopAllCoroutines();
		StartCoroutine(MoveLeftCursor());
	}

    public void NextAbility()
    {
        if (currentAbility != null) abilities.AddLast(currentAbility);
		currentAbility = abilities.First.Value;
		abilities.RemoveFirst();
        AbilityHandler abilityHandler = player.GetAbilityHandler();
        abilityHandler.ActivateAbilityByType(System.Type.GetType(currentAbility.abilityName));
        eventSystem.SetSelectedGameObject(abilityImage.gameObject);
		abilityImage.sprite = currentAbility.abilitySprite;
        abilityBackground.transform.localScale = Vector2.one * defaultAbilityScale * 1.3f;
		StopAllCoroutines();
		StartCoroutine(MoveRightCursor());
	}

    public void PreviousAbility()
    {
		if (currentAbility != null) abilities.AddFirst(currentAbility);
		currentAbility = abilities.Last.Value;
		abilities.RemoveLast();
        AbilityHandler abilityHandler = player.GetAbilityHandler();
        abilityHandler.ActivateAbilityByType(System.Type.GetType(currentAbility.abilityName));
        eventSystem.SetSelectedGameObject(abilityImage.gameObject);
        abilityImage.sprite = currentAbility.abilitySprite;
		abilityBackground.transform.localScale = Vector2.one * defaultAbilityScale * 1.3f;
		StopAllCoroutines();
		StartCoroutine(MoveLeftCursor());
	}
    private void ActivateAcceptInputs()
    {
        acceptInputs = true;
	}

    public void ConfirmCharacterSelection()
    {
        if (!ready && acceptInputs)
        {
            ready = true;
            readyForeground.gameObject.SetActive(true);
            readyText.fontSize = 3;
            multiplayerManager.CheckAllPlayersReady();
        }
    }

    public void CancelCharacterSelection()
    {
        if (ready)
        {
            ready = false;
            readyForeground.gameObject.SetActive(false);
            multiplayerManager.CheckAllPlayersReady();
        }
    }

    public void OnPlayerLost()
    {
        multiplayerManager.AddSkinToFront(playerSprite.sprite);
        multiplayerManager.CheckAllPlayersReady();
        Destroy(parent);
    }
    public Color GetCurrentTeam()
    {
        return currentTeam;
    }
    public bool IsReady { get { return ready; } }
    

    private void BouncePlayer()
    {
        player.transform.position = defaultPlayerPos;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    private IEnumerator MoveLeftCursor()
    {
        cursorLeft.transform.localPosition = new(cursorLeftX - 5, cursorLeft.transform.localPosition.y);

        while (cursorLeft.transform.localPosition.x != cursorLeftX)
        {
            cursorLeft.transform.localPosition = new(Mathf.MoveTowards(cursorLeft.transform.localPosition.x, cursorLeftX, 50 * Time.deltaTime), cursorLeft.transform.localPosition.y);
            yield return null;
        }
    }
    private IEnumerator MoveRightCursor()
    {
        cursorRight.transform.localPosition = new(cursorRightX + 5, cursorRight.transform.localPosition.y);

        while (cursorRight.transform.localPosition.x != cursorRightX)
        {
            cursorRight.transform.localPosition = new(Mathf.MoveTowards(cursorRight.transform.localPosition.x, cursorRightX, 50 * Time.deltaTime), cursorRight.transform.localPosition.y);
            yield return null;
        }
    }
}
