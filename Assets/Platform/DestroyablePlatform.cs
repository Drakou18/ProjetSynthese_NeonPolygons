using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System.Linq;

public class DestroyablePlatform : MonoBehaviour
{
	private const float colliderRefreshRate = 0.1f;

	private SpriteRenderer spriteRenderer;
	private Texture2D initialTexture;
	private Texture2D texture;
	private PolygonCollider2D polygonCollider;
	private SpriteMask spriteMask;
	private int pixelCount = 100;
	private SpritePixelCollider2D pixelPerfectColliderScript;
	private Joint2D joint;
	private Rigidbody2D rb;
	private Vector2 originalPosition;
	private bool isLocked = false;
	private bool willUpdateCollider = false;
	[SerializeField] private Pixel pixelPrefab;

    void Awake()
	{
		originalPosition = transform.localPosition;
		pixelPerfectColliderScript = GetComponent<SpritePixelCollider2D>();
		spriteRenderer = GetComponent<SpriteRenderer>();
        initialTexture = DuplicateTexture(spriteRenderer.sprite.texture);
        texture = DuplicateTexture(initialTexture);
        spriteRenderer.sprite = Sprite.Create(texture, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), spriteRenderer.sprite.pixelsPerUnit);
		polygonCollider = GetComponent<PolygonCollider2D>();
		joint = GetComponent<Joint2D>();
		rb = GetComponent<Rigidbody2D>();
		UpdatePixelCount();
		CheckPixelCount(0);
		spriteMask = GetComponent<SpriteMask>();
		spriteMask.sprite = spriteRenderer.sprite;
	}

	private void UpdateCollider()
	{
		pixelPerfectColliderScript.Regenerate();
		willUpdateCollider = false;
		if (polygonCollider.pathCount > 1) CheckForSplit();
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.CompareTag(Harmony.Tags.DestroyArea) && !isLocked)
			RemovePixels(collision);
	}

	public Vector2 GetOriginalPosition()
	{
		return originalPosition;
	}

	public void SetLocked(bool isLocked)
	{
		this.isLocked = isLocked;
	}

	public int GetPixelCount()
	{
		return pixelCount;
	}

    public void SetOriginalTexture()
    {
        texture = DuplicateTexture(initialTexture);
        spriteRenderer.sprite = Sprite.Create(texture, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), spriteRenderer.sprite.pixelsPerUnit);
        spriteMask = GetComponent<SpriteMask>();
        spriteMask.sprite = spriteRenderer.sprite;

		if (joint == null)
		{
			SpringJoint2D springJoint = transform.AddComponent<SpringJoint2D>();
			springJoint.enableCollision = true;
			springJoint.autoConfigureConnectedAnchor = true;
			springJoint.distance = 0.005f;
			joint = springJoint;
		}

        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
		rb.drag = 5.0f;

        pixelPerfectColliderScript.Regenerate();
        UpdatePixelCount();
    }

    private Texture2D DuplicateTexture(Texture2D original)
	{
        Texture2D duplicate = new Texture2D(original.width, original.height, TextureFormat.RGBA32, false);
		duplicate.filterMode = FilterMode.Point;
		Graphics.CopyTexture(original, duplicate);

        return duplicate;

    }

	private void RemovePixels(Collider2D collider)
	{
        Vector3 spriteCenterInWorld = spriteRenderer.bounds.center;
		Vector3 colliderCenterInWorld = collider.bounds.center;

		Vector3 centerOffsetLocal = transform.InverseTransformPoint(colliderCenterInWorld) - transform.InverseTransformPoint(spriteCenterInWorld);
		float pixelsPerUnit = spriteRenderer.sprite.pixelsPerUnit;

		Vector2 textureCenter = new Vector2(texture.width / 2, texture.height / 2);
		Vector2 colliderCenterInTexture = textureCenter + new Vector2(centerOffsetLocal.x * pixelsPerUnit, centerOffsetLocal.y * pixelsPerUnit);

		Vector2 colliderSizeInTexture = new Vector2(collider.bounds.size.x, collider.bounds.size.y);

		int leftBound = Mathf.Clamp(Mathf.RoundToInt(colliderCenterInTexture.x - colliderSizeInTexture.x / 2), 0, texture.width);
		int bottomBound = Mathf.Clamp(Mathf.RoundToInt(colliderCenterInTexture.y - colliderSizeInTexture.y / 2), 0, texture.height);
		int rightBound = Mathf.Clamp(Mathf.RoundToInt(colliderCenterInTexture.x + colliderSizeInTexture.x / 2), 0, texture.width);
		int topBound = Mathf.Clamp(Mathf.RoundToInt(colliderCenterInTexture.y + colliderSizeInTexture.y / 2), 0, texture.height);

		int width = rightBound - leftBound;
		int height = topBound - bottomBound;

		if (width <= 0 || height <= 0) return;

		Color[] pixels = texture.GetPixels(leftBound, bottomBound, width, height);
		bool removedPixels = false;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				Vector2 pixelPoint = new Vector2(x + leftBound, y + bottomBound);
				Vector2 worldPoint = GetPixelToWorld((int)pixelPoint.x, (int)pixelPoint.y);

				if (collider.OverlapPoint(worldPoint))
				{
					int index = y * width + x;
					if (pixels[index].a > 0f)
					{
						removedPixels = true;
                        Vector2 directionFromColliderToPixel = (worldPoint - (Vector2)collider.transform.position).normalized;
                        DisassemblePixel(worldPoint, pixels[index], directionFromColliderToPixel);
                        pixels[index] = new Color(0, 0, 0, 0);
						ReducePixelCountBy(1);
					}
				}
			}
		}

		if (removedPixels)
		{
			texture.SetPixels(leftBound, bottomBound, width, height, pixels);
			texture.Apply();
			
			if (!willUpdateCollider)
			{
				willUpdateCollider = true;
				Invoke("UpdateCollider", colliderRefreshRate);
			}
		}
    }

	public void CheckForSplit()
	{
		if (polygonCollider.pathCount > 1)
		{
			Vector2[] mainPath = polygonCollider.GetPath(0);
			List<Vector2[]> remainingPaths = new();
			remainingPaths.Add(mainPath);

			GameObject tempObject = new GameObject(Harmony.GameObjects.TempCollider);
			PolygonCollider2D tempCollider = tempObject.AddComponent<PolygonCollider2D>();
			tempCollider.pathCount = 1;
			tempCollider.SetPath(0, mainPath);

			for (int i = 1; i < polygonCollider.pathCount; i++)
			{
				Vector2[] currentPath = polygonCollider.GetPath(i);
				bool isContained = true;
				int nbOfExternalPoints = 0;

				foreach (Vector2 point in currentPath)
				{
					if (!tempCollider.OverlapPoint(point))
					{
						nbOfExternalPoints++;
						if (nbOfExternalPoints >= 4)
						{
							isContained = false;
							break;
						}
					}
				}

				if (isContained)
				{
					remainingPaths.Add(currentPath);
				}
				else
				{
					DestroyablePlatform newPlatform = Instantiate(gameObject).GetComponent<DestroyablePlatform>();
                    newPlatform.initialTexture = initialTexture;
                    newPlatform.originalPosition = originalPosition;
					newPlatform.transform.parent = transform.parent;
					newPlatform.polygonCollider.pathCount = 1;
					newPlatform.polygonCollider.SetPath(0, currentPath);
					newPlatform.RemoveSplitPixels();
				}
			}

			Destroy(tempObject);

			polygonCollider.pathCount = 0;
			polygonCollider.pathCount = remainingPaths.Count;
			for (int i = 0; i < polygonCollider.pathCount; i++) polygonCollider.SetPath(i, remainingPaths[i]);
			RemoveSplitPixels();
		}
	}

    private void RemoveSplitPixels()
    {
        float pixelsPerUnit = spriteRenderer.sprite.pixelsPerUnit;
        Matrix4x4 worldToLocalMatrix = transform.worldToLocalMatrix;

        Vector3 spriteCenterInWorld = spriteRenderer.bounds.center;
        Vector3 colliderCenterInWorld = polygonCollider.bounds.center;

        Vector3 centerOffsetLocal = worldToLocalMatrix.MultiplyPoint3x4(colliderCenterInWorld) - worldToLocalMatrix.MultiplyPoint3x4(spriteCenterInWorld);

        Vector2 textureCenter = new Vector2(texture.width / 2, texture.height / 2);
        Vector2 colliderCenterInTexture = textureCenter + new Vector2(centerOffsetLocal.x * pixelsPerUnit, centerOffsetLocal.y * pixelsPerUnit);

        Vector2[] localPoints = polygonCollider.GetPath(0);
        Vector2 min = localPoints[0];
        Vector2 max = localPoints[0];

        foreach (Vector2 point in localPoints)
        {
            min = Vector2.Min(min, point);
            max = Vector2.Max(max, point);
        }

        Vector2 size = max - min;
        Vector2 colliderSizeInTexture = new Vector2(size.x * pixelsPerUnit, size.y * pixelsPerUnit);


        int leftBound = Mathf.Clamp(Mathf.FloorToInt(colliderCenterInTexture.x - colliderSizeInTexture.x / 2), 0, texture.width);
        int bottomBound = Mathf.Clamp(Mathf.FloorToInt(colliderCenterInTexture.y - colliderSizeInTexture.y / 2), 0, texture.height);
        int rightBound = Mathf.Clamp(Mathf.CeilToInt(colliderCenterInTexture.x + colliderSizeInTexture.x / 2), 0, texture.width);
        int topBound = Mathf.Clamp(Mathf.CeilToInt(colliderCenterInTexture.y + colliderSizeInTexture.y / 2), 0, texture.height);

        int width = rightBound - leftBound;
        int height = topBound - bottomBound;

        Color transparent = new Color(0, 0, 0, 0);
        if (width <= 0 || height <= 0) return;

        Color[] pixels = texture.GetPixels(leftBound, bottomBound, width, height);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int i = y * width + x;
                if (pixels[i].a > 0)
                {

                    int pixelCenterX = Mathf.RoundToInt(x + 0.5f);
                    int pixelCenterY = Mathf.RoundToInt(y + 0.5f);

                    Vector2 worldPoint = GetPixelToWorld(pixelCenterX + leftBound, pixelCenterY + bottomBound);


                    float edgeThreshold = 0.5f;

                    if (!polygonCollider.OverlapPoint(worldPoint) && Vector2.Distance(polygonCollider.ClosestPoint(worldPoint), worldPoint) > edgeThreshold)
                    {
                        pixels[i] = transparent;
                    }
                }
            }
        }

        Color[] clearCanvas = new Color[texture.width * texture.height];
        texture.SetPixels(clearCanvas);
        texture.SetPixels(leftBound, bottomBound, width, height, pixels);
        texture.Apply();

        int oldPixelCount = pixelCount;
        pixelCount = texture.GetPixels().Count(p => p.a > 0);
        CheckPixelCount(oldPixelCount);
    }

    private void DisassemblePixel(Vector2 pos, Color color, Vector2 dir)
    {
        Color actualColor = color;
        if (actualColor == Color.black || actualColor == Color.white) actualColor = spriteRenderer.color;
        PixelManager.Instance.GetPixel().Initialize(pos, (1f / spriteRenderer.sprite.pixelsPerUnit) * transform.localScale.x, actualColor, dir);
    }

    private void CheckPixelCount(int oldPixelCount)
	{
		if (pixelCount <= 15) 
		{
			foreach(Transform child in  transform) child.parent = transform.parent;
			Destroy(gameObject);
		} 

		if ((pixelCount < oldPixelCount / 2 && joint != null) || pixelCount <= 100)
		{
			if (rb.constraints != RigidbodyConstraints2D.FreezeAll)
			{
				Destroy(joint);
				rb.constraints = RigidbodyConstraints2D.None;
				rb.drag = 0;
				rb.gravityScale = 5;
			}
		}
	}

	private void ReducePixelCountBy(int nb)
	{
		pixelCount-=nb;
		CheckPixelCount(0);	
	}

	private void UpdatePixelCount()
	{
		pixelCount = texture.GetPixels().Count(p => p.a > 0);
		CheckPixelCount(pixelCount);
	}

	private Vector2 GetPixelToWorld(int x, int y)
	{
		Vector3 localPos = new Vector3((float)x / texture.width, (float)y / texture.height, 0);
		localPos.x *= spriteRenderer.sprite.rect.width / spriteRenderer.sprite.pixelsPerUnit;
		localPos.y *= spriteRenderer.sprite.rect.height / spriteRenderer.sprite.pixelsPerUnit;
		localPos -= (Vector3)spriteRenderer.sprite.pivot / spriteRenderer.sprite.pixelsPerUnit;
		return transform.TransformPoint(localPos);
	}

	public bool GetIsLocked()
	{
		return isLocked;
	}
}
