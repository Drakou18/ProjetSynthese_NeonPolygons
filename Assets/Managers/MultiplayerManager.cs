using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MultiplayerManager : MonoBehaviour
{
	public static MultiplayerManager Instance { get; private set; }
	[SerializeField] private Transform canvas;
	[SerializeField] private List<Sprite> playerSkins;
    [SerializeField] private List<Color> teamColors;
	[SerializeField] private List<AbilitySelection> abilities;
	[SerializeField] private TextMeshProUGUI countdown;
	[SerializeField] private GameObject countdownBanner;
	[SerializeField] private float weaponSwitchingInterval = 15f;
	[SerializeField] private bool isWeaponSwitchingActive = false;
	[SerializeField] private int pointsToWin = 10;
	[SerializeField] private float timeBeforeGiveWeaponEnd = 3f;
	private float timeUntilNextWeaponSwitch = 0;
	private RectTransform winnerBanner;
	private RectTransform roundStatsBanner;
    private LinkedList<Sprite> playerSkinsLinkedList = new LinkedList<Sprite>();
	private List<GameObject> playersSelection = new();
	private List<GameObject> allLevels;
	private GameObject currentLevel;
	public bool roundEnded = false;
	public List<Player> players = new();

    [SerializeField] private System.Type firstPlaceEndWeapon = typeof(RocketLauncher);
    [SerializeField] private System.Type secondPlaceEndWeapon = typeof(Ak47);
    [SerializeField] private System.Type thirdPlaceEndWeapon = typeof(Shotgun);
    [SerializeField] private System.Type otherEndWeapon = typeof(Handgun);

    List<Vector2> slots = new List<Vector2>() {
		new(-120,40), new(-40,40), new(40,40), new(120, 40),
		new(-120,-40), new(-40,-40), new(40,-40), new(120,-40),
	};

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);

			allLevels = Resources.LoadAll<GameObject>("Levels").ToList();
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		playerSkins.ForEach(skin => { playerSkinsLinkedList.AddFirst(skin); });
    }

    private void Update()
    {
		if (countdown != null && countdown.isActiveAndEnabled) countdown.fontSize = Mathf.MoveTowards(countdown.fontSize, 36, Time.deltaTime * 40f);

		if (isWeaponSwitchingActive)
		{
			timeUntilNextWeaponSwitch -= Time.deltaTime;
			if (timeUntilNextWeaponSwitch < 0 && !roundEnded)
			{
				timeUntilNextWeaponSwitch = weaponSwitchingInterval;
				GameUI.Instance.ShowSwitchText(1f);
				players.ForEach(p => p.EquipRandomWeapon());
			}
		}
    }

    private void OnPlayerJoined(PlayerInput playerInput)
	{
        DontDestroyOnLoad(playerInput.gameObject);
        playerInput.transform.SetParent(canvas, false);
        playerInput.transform.position = slots[playerInput.playerIndex];
        playersSelection.Add(playerInput.gameObject);
    }

	public void RemoveAllPlayers()
	{
        foreach (GameObject obj in playersSelection)
        {
            Destroy(obj);
        }
    }

	public void CheckAllPlayersReady()
	{
		List<CharacterSelectionMenu> menus = canvas.GetComponentsInChildren<CharacterSelectionMenu>().ToList();
		Color nullTeam = new(0, 0, 0, 0);
		Color team1 = nullTeam;
		bool atLeastTwoTeams = false;
		bool allReady = true;
		foreach (CharacterSelectionMenu menu in menus)
		{
			if (!menu.IsReady)
			{
				allReady = false;
				break;
			}
			if (team1 == nullTeam) team1 = menu.GetCurrentTeam();
			else if (team1 != menu.GetCurrentTeam()) atLeastTwoTeams = true;
		}

		if (allReady && atLeastTwoTeams && menus.Count > 1)
		{
			StopAllCoroutines();
			StartCoroutine(Countdown());
		}
		else
		{
			StopAllCoroutines();
			countdownBanner.gameObject.SetActive(false);
		}
	}

	private IEnumerator Countdown()
	{
		countdownBanner.gameObject.SetActive(true);
		int timeLeft = 3;

		while (timeLeft > 0)
		{
			countdown.text = timeLeft.ToString();
			countdown.fontSize = 46f;
			yield return new WaitForSeconds(1);
			timeLeft--;
		}
		countdownBanner.gameObject.SetActive(false);

		List<CharacterSelectionMenu> menus = canvas.GetComponentsInChildren<CharacterSelectionMenu>().ToList();
		players = new List<Player>();
		foreach (CharacterSelectionMenu menu in menus)
		{
			PlayerInput playerInput = menu.transform.parent.GetComponent<PlayerInput>();
			playerInput.transform.SetParent(transform);

			Player playerCharacter = playerInput.GetComponentInChildren<Player>();
			playerCharacter.GetComponent<Rigidbody2D>().gravityScale = 60;
			playerCharacter.defaultGravity = 60;
			playerCharacter.Jump();
			players.Add(playerCharacter);

			playerCharacter.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "Default";

			Destroy(menu.transform.Find(Harmony.GameObjects.Platform).gameObject);
			Destroy(menu.gameObject);
		}
		Destroy(canvas.gameObject);
		GetComponent<PlayerInputManager>().joinBehavior = PlayerJoinBehavior.JoinPlayersManually;

		yield return new WaitForSeconds(1.5f);

		foreach(Player player in players)
		{
			player.transform.localScale = Vector3.one * 1.2f;
            player.GetComponentInParent<PlayerInput>().SwitchCurrentActionMap("Player");
        }

		SceneController.Instance.LoadSceneAsync("Scenes/GameScene", LoadSceneMode.Single, StartFirstRound);

		yield return new WaitForSeconds(1.5f);

        isWeaponSwitchingActive = true;
    }

	private void StartFirstRound(AsyncOperation obj) {
		SetupGameUI();
        LoadRandomLevel();
	}

	private void SetupGameUI() {
		GameUI gameUI = FindAnyObjectByType(typeof(GameUI)).GetComponent<GameUI>();
		gameUI.CreateUI();
	}

	private void SetPlayerPositions()
	{
		SpawnPointManager spawnPointManager = FindFirstObjectByType<SpawnPointManager>();

		for (int i = 0; i < players.Count; i++) {
			players[i].transform.position = spawnPointManager.GetSpawnPoint(i);
		}
	}


	private void SetPlayerPositionsOnPodium() {
        SpawnPointManager spawnPointManager = FindFirstObjectByType<SpawnPointManager>();

		List<int> scores = new List<int>();

		// [5, 5, 3, 3, 3, 2, 1] => [5, 3, 2, 1]
        for (int i = 0; i < players.Count; i++)
        {
			if (!scores.Contains(players[i].GetPoints()))
				scores.Add(players[i].GetPoints());
        }

        for (int i = 0; i < players.Count; i++)
        {
			int spawnIndex = scores.FindIndex(x => x == players[i].GetPoints());
            players[i].transform.position = spawnPointManager.GetSpawnPoint(spawnIndex);
        }
    }


    public Sprite GetNextAvailableSkin()
	{
		Sprite skin = playerSkinsLinkedList.First.Value;
		playerSkinsLinkedList.RemoveFirst();
		return skin;
	}
	public Sprite GetPreviousAvailableSkin()
	{
		Sprite skin = playerSkinsLinkedList.Last.Value;
		playerSkinsLinkedList.RemoveLast();
		return skin;
	}

	public void AddSkinToFront(Sprite sprite)
	{
		playerSkinsLinkedList.AddFirst(sprite);
	}

	public void AddSkinToBack(Sprite sprite)
	{
		playerSkinsLinkedList.AddLast(sprite);
	}

	public List<Color> GetTeamColors()
	{
		return teamColors;
	}

	public Color GetTeam(int teamNb)
	{
		return teamColors.ElementAt(teamNb);
	}

	public int GetTeamNumber(Color color)
	{
		return teamColors.IndexOf(color) + 1;
	}

	public List<AbilitySelection> GetAbilities()
	{
		return abilities;
	}

	public List<Player> GetAllPlayers() {
		return this.players;
	}

	public void EndRound(List<Player> winners)
	{
		StartCoroutine(EndRoundCoroutine(winners));
	}

	private IEnumerator EndRoundCoroutine(List<Player> winners)
	{
		roundEnded = true;
		foreach (Player winner in winners) {
			winner.AddPoints(1);
		}

        if (winnerBanner == null) winnerBanner = GameObject.Find(Harmony.GameObjects.GameUI).transform.Find(Harmony.GameObjects.WinnerBanner).GetComponent<RectTransform>();
		if (roundStatsBanner == null) roundStatsBanner = GameObject.Find(Harmony.GameObjects.GameUI).transform.Find(Harmony.GameObjects.RoundStatsBanner).GetComponent<RectTransform>();
		while (winnerBanner.anchoredPosition.y > 500 || roundStatsBanner.anchoredPosition.y < -500)
		{
			winnerBanner.anchoredPosition = new(0, Mathf.MoveTowards(winnerBanner.anchoredPosition.y, 500, Time.deltaTime * 100f));
			roundStatsBanner.anchoredPosition = new(0, Mathf.MoveTowards(roundStatsBanner.anchoredPosition.y, -500, Time.deltaTime * 100f));
			yield return null;
		}
		yield return new WaitForSeconds(2);
		while (winnerBanner.anchoredPosition.y > 400 || roundStatsBanner.anchoredPosition.y < -400)
		{
			winnerBanner.anchoredPosition = new(0, Mathf.MoveTowards(winnerBanner.anchoredPosition.y, 400, Time.deltaTime * 600f));
			roundStatsBanner.anchoredPosition = new(0, Mathf.MoveTowards(roundStatsBanner.anchoredPosition.y, -400, Time.deltaTime * 600f));
			yield return null;
		}


		PoolManager.Instance.DisableAll();

        if (winners[0].GetPoints() >= pointsToWin)
		{
			LoadEndGame();
			yield return new WaitForSeconds(timeBeforeGiveWeaponEnd);

			List<int> scores = new List<int>();

            // [5, 5, 3, 3, 3, 2, 1] => [5, 3, 2, 1]
            for (int i = 0; i < players.Count; i++)
            {
                if (!scores.Contains(players[i].GetPoints()))
                    scores.Add(players[i].GetPoints());
            }


            for (int i = 0; i < players.Count; i++)
			{
                switch (scores.FindIndex(x => x == players[i].GetPoints()))
                {
                    case 0:
                        players[i].EquipeWeaponByType(firstPlaceEndWeapon);
                        break;
                    case 1:
                        players[i].EquipeWeaponByType(secondPlaceEndWeapon);
                        break;
                    case 2:
                        players[i].EquipeWeaponByType(thirdPlaceEndWeapon);
                        break;
                    default:
                        players[i].EquipeWeaponByType(otherEndWeapon);
                        break;
                }
                
            }
        }
		else 
		{
			LoadRandomLevel();
            yield return new WaitForSeconds(1f);
            while (winnerBanner.anchoredPosition.y < 625 || roundStatsBanner.anchoredPosition.y > -625)
            {
                winnerBanner.anchoredPosition = new(0, Mathf.MoveTowards(winnerBanner.anchoredPosition.y, 625, Time.deltaTime * 1400f));
                roundStatsBanner.anchoredPosition = new(0, Mathf.MoveTowards(roundStatsBanner.anchoredPosition.y, -625, Time.deltaTime * 1400f));
                yield return null;
            }
        }
	}

    public void SortPlayersByScore()
    {
        this.players = this.players.OrderByDescending(obj => obj.GetPoints()).ToList();
    }

    public void CheckForWinner()
	{
		if (!roundEnded)
		{
			List<Player> winners = new List<Player>();
			bool foundTeam = false;
			Color winningTeam = Color.black;
			foreach (Player player in this.players)
			{
				if (player.gameObject.activeSelf && !foundTeam) {
					winners.Add(player);
					foundTeam = true;
					winningTeam = player.GetTeam();
				}
				else if (player.gameObject.activeSelf && foundTeam && player.GetTeam() != winningTeam) {
					foundTeam = false;
					break;
				}
				else if (player.gameObject.activeSelf && foundTeam && player.GetTeam() == winningTeam) {
					winners.Add(player);
				}
			}

			if (foundTeam)
			{
				roundEnded = true;
				EndRound(winners);
			}
		}
	}

    private void LoadRandomLevel()
	{
		timeUntilNextWeaponSwitch = 4;
		roundEnded = false;
		if (currentLevel != null) Destroy(currentLevel);
		currentLevel = Instantiate(allLevels[Random.Range(0, allLevels.Count)]);

		foreach(Player player in players)
		{
			player.gameObject.SetActive(true);
            player.UnequipAllWeapons();
            player.ShowInGameHUD();
			player.SetDamage(1);
		}
		SetPlayerPositions();
	}

	private void LoadEndGame() {
        timeUntilNextWeaponSwitch = 100000;
        roundEnded = false;
        SceneController.Instance.LoadSceneAsync("Scenes/EndGameScene", LoadSceneMode.Single, SetPlayerPositionsAsync);
    }

	private void SetPlayerPositionsAsync(AsyncOperation obj) {
		SortPlayersByScore();
        SetPlayerPositionsOnPodium();
        foreach (Player player in players)
        {
            player.gameObject.SetActive(true);
            player.UnequipAllWeapons();
            player.SetDamage(1);
			player.FreezeInputsFor(timeBeforeGiveWeaponEnd);
        }
    }
}

[System.Serializable]
public class AbilitySelection
{
	public string abilityName;
	public Sprite abilitySprite;
}
