using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEngine.UI.CanvasScaler;

public class SceneController : MonoBehaviour
{
    public static SceneController Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void LoadSceneAsync(string sceneName, LoadSceneMode sceneMode, Action<AsyncOperation> function)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, sceneMode);
        if (function != null)
            asyncLoad.completed += function;
    }

    public void LoadRandomSceneAsync(List<string> sceneNames, LoadSceneMode sceneMode, Action<AsyncOperation> function) {
        string randomSceneName = GetRandomElement(sceneNames);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(randomSceneName, sceneMode);
        if (function != null)
            asyncLoad.completed += function;
    }

    public void UnloadSceneAsync(string sceneName, Action<AsyncOperation> function)
    {
        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(sceneName);
        if (function != null)
            asyncUnload.completed += function;
    }

    private static T GetRandomElement<T>(List<T> list)
    {
        if (list.Count > 0) {
            int randomIndex = new System.Random().Next(0, list.Count);
            return list[randomIndex];
        }
        return default(T);
    }
}
