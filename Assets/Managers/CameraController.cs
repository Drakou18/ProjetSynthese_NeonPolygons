using Harmony;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float panningSpeed = 1.0f;
    [SerializeField] private float zoomingSpeed = 1.0f;
    [SerializeField] private float padding = 50;
    private float initialWidth = 0;
    private float initialHeight = 0;
    private Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
        if (cam) {
            Vector2 bottomLeft = cam.BottomLeftWorldPoint();
            Vector2 topRight = cam.TopRightWorldPoint();

            this.initialWidth = topRight.x - bottomLeft.x;
            this.initialHeight = topRight.y - bottomLeft.y;
        }
    }
    void Update()
    {
        List<Rigidbody2D> players = MultiplayerManager.Instance.players.Select(player => player.GetComponent<Rigidbody2D>()).ToList(); ;
        if (players.Count(p => p.gameObject.activeInHierarchy) > 0)
        {
			float minX = Mathf.Max(players.Where(p => p != null && p.gameObject.activeInHierarchy).Min(p => p.transform.position.x), -initialWidth / 2f);
			float maxX = Mathf.Min(players.Where(p => p != null && p.gameObject.activeInHierarchy).Max(p => p.transform.position.x), initialWidth / 2f);
			float minY = Mathf.Max(players.Where(p => p != null && p.gameObject.activeInHierarchy).Min(p => p.transform.position.y), -initialHeight / 2f);
			float maxY = Mathf.Min(players.Where(p => p != null && p.gameObject.activeInHierarchy).Max(p => p.transform.position.y), initialHeight / 2f);
			float maxVelocity = players.Where(p => p != null).Max(p => p.velocity.magnitude);

			Vector2 averagePlayerPosition = new((minX + maxX) / 2f, (minY + maxY) / 2f);
            float distanceToTarget = Vector2.Distance(transform.position, averagePlayerPosition) / 150f;



			float targetSize = Mathf.Clamp(Mathf.Pow(Mathf.Max((maxY - minY + padding * 2) / 2f, (maxX - minX + padding * 2) / 2f / 16f * 9f), 1.02f), 65, 180);
			cam.orthographicSize = Mathf.MoveTowards(cam.orthographicSize, targetSize, Mathf.Abs(zoomingSpeed * (targetSize - cam.orthographicSize) / 100));
			transform.position = new(Mathf.MoveTowards(transform.position.x, averagePlayerPosition.x, panningSpeed * distanceToTarget), Mathf.MoveTowards(transform.position.y, averagePlayerPosition.y, panningSpeed * distanceToTarget));
		}
	}
}
